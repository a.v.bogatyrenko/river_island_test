import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.core.IsNull;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class FirstApiTest {

    private ElementsOfCatsImg[] catsImgItems;
    private int afterPostId = 0;

    @BeforeClass
    void setup() {
        RestAssured.baseURI = "https://api.thecatapi.com/v1";
    }

    @Test
    void objective1() {
        Response r = given().log().all()
                .header("x-api-key", "DEMO-API-KEY")
                .get("/votes");

        r.then().statusCode(200);
        catsImgItems = r.body().as(ElementsOfCatsImg[].class);
        Assert.assertTrue(catsImgItems.length > 0);
    }

    @Test(dependsOnMethods = "objective1")
    void objective2() {
        int randomId = new Random().nextInt(catsImgItems.length);

        given().log().all()
                .header("x-api-key", "DEMO-API-KEY")
                .get("/votes/{id}", catsImgItems[randomId].id)
                .then()
                .statusCode(200)
                .body("id", equalTo(catsImgItems[randomId].id))
                .body("image_id", equalTo(catsImgItems[randomId].image_id))
                .body("sub_id", equalTo(catsImgItems[randomId].sub_id))
                .body("created_at", equalTo(catsImgItems[randomId].created_at))
                .body("value", equalTo(catsImgItems[randomId].value))
                .body("country_code", equalTo(catsImgItems[randomId].country_code));
    }

    @Test
    void objective3() {

        String body = "{\n" +
                "\"image_id\": \"asf3\",\n" +
                "\"sub_id\": \"my-user-1234\",\n" +
                "\"value\": 1\n" +
                "}";

        Response r = given().log().all()
                .header("x-api-key", "DEMO-API-KEY")
                .contentType(ContentType.JSON)
                .body(body)
                .post("/votes")
                .then()
                .statusCode(200)
                .extract()
                .response();

        afterPostId = r.path("id");
        Assert.assertEquals(r.path("message"), "SUCCESS");
        Assert.assertNotEquals(afterPostId, is(IsNull.nullValue()));
    }

    @Test(dependsOnMethods = "objective3")
    void objective4() {

        given().log().all()
                .header("x-api-key", "DEMO-API-KEY")
                .get("/votes/{id}", afterPostId)
                .then()
                .statusCode(200)
                .body("id", equalTo(afterPostId));
    }

    @Test(dependsOnMethods = {"objective3", "objective4"})
    void objective5() {

        given().log().all()
                .header("x-api-key", "DEMO-API-KEY")
                .delete("/votes/{id}", afterPostId)
                .then()
                .body("message", equalTo("SUCCESS"));
    }

    @Test(dependsOnMethods = {"objective3", "objective4", "objective5"})
    void objective6() {

        given().log().all()
                .header("x-api-key", "DEMO-API-KEY")
                .get("/votes/{id}", afterPostId)
                .then()
                .statusCode(404)
                .body("message", equalTo("NOT_FOUND"));
    }

}
